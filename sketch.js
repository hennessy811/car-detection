var video = document.getElementById('video');
var videoStatus = document.getElementById('videoStatus');
var loading = document.getElementById('loading');
var result = document.getElementById('result');
var confidence = document.getElementById('confidence');
var predict = document.getElementById('predict');

// A variable to store the total loss
let totalLoss = 0;
let isCat = false;
let prevState = false;
let state = false;
let shouldUpdate = true;
let isProcessing = false;

// let d = debounce(() => {
//   isCat ? open() : close();
// }, 5000);

// Create a webcam capture

navigator.mediaDevices.enumerateDevices().then(res => {
  return res.find(item => item.label.includes('Microsoft'))
}).then(res => {
  navigator.mediaDevices.getUserMedia({ video: {
      deviceId: res.deviceId,
      frameRate: {
        ideal: 1,
        max: 2,
        min: 0.5,
      }
    } })
    .then((stream) => {
      video.srcObject = stream;
      video.play();
    })

});

// A function to be called when the model has been loaded
function modelLoaded() {
  loading.innerText = 'Model loaded!';
}

// Extract the already learned features from MobileNet
const featureExtractor = ml5.featureExtractor('MobileNet', modelLoaded);
// Create a new classifier using those features
featureExtractor.loadModel('model.weights.bin');
const classifier = featureExtractor.classification(video, videoReady);

classifier.load('model.json');
// classifier.loadWeights('model.weights.bin');

// Predict the current frame.
function predict() {
  classifier.predict(gotResults);
}

// A function to be called when the video is finished loading
function videoReady() {
  videoStatus.innerText = 'Video ready!';
}

// Show the results
function gotResults(err, results) {
  // Display any error
  if (err) {
    console.error(err);
  }
  if (results && results[0]) {
    result.innerText = results[0].label;
    isCat = results[0].label === 'cat';

    state = isCat;
    if (prevState === state) {}
    else {
      prevState = state;
      // d();
      console.log(isCat);
      isCat ? open() : close();
    }

    shouldUpdate = false;
    // if (isCat)

    confidence.innerText = results[0].confidence;
    classifier.classify(gotResults);
  }
}

// Start predicting when the predict button is clicked
predict.onclick = function () {
  classifier.classify(gotResults);
};

function debounce(f, ms) {

  let isCooldown = false;

  return function() {
    if (isCooldown) return;

    f.apply(this, arguments);

    isCooldown = true;

    setTimeout(() => isCooldown = false, ms);
  };

}

const open = () => {
  if (isProcessing) return;
  else {
    isProcessing = true;
    setTimeout(() => {
      console.log('OPEN THE DOORS!');
      fetch('http://10.200.22.230:5000/open');
      isProcessing = false;
    }, 3000)
  }
};

const close = () => {
  if (isProcessing) return;
  else {
    isProcessing = true;
    setTimeout(() => {
      console.log('CLOSE THE DOORS!');
      fetch('http://10.200.22.230:5000/close');
      isProcessing = false;
    }, 3000)
  }
  // console.log('CLOSE THE DOORS!');
};
